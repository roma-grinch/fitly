import { Module } from '@nestjs/common';
import { LinkController } from './controllers';
import { LinkService } from './services';

@Module({
  imports: [],
  controllers: [LinkController],
  providers: [LinkService],
})
export class LinkModule {}
