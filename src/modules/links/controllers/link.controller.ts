import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { LinkService } from '../services';

@Controller()
export class LinkController {
  constructor(private readonly linkService: LinkService) {}

  @Post('/create-short-link')
  async createShortLink(
    @Body('link')
    link,
  ) {
    console.log('-A-1-----------------------------------');
    console.log(link);
    const shortLink = await this.linkService.createShortLink(link);
    console.log('-A-3-----------------------------------');
    console.log(shortLink);
    return shortLink;
  }
  // @Post('/save-short-link')
  // saveShortLink() {
  //   console.log()
  //   return ;
  // }
}

// @Post(':link')
// createShortLink(
//   @Param('link')
//   link: string,
// ): string {
//   return 'link created';
// }

// @Put('blabla/:id/egogo/:user')

//   @Put('blabla/:id/egogo/:user')
//   linkEdit(
//     @Param('id')
//     id,
//     @Param('user')
//     user,
//   ) {
//     console.log(id, user)
//     return 'ok';
//   }

// @Post(':link')
// createShortLink(
//   @Param('link')
//   link,
// ) {
//   console.log(link);
//   return 'ok';
// }
