import { PrismaService } from '@common/prisma';
import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class LinkService {
  constructor(private prisma: PrismaService) {}

  async createShortLink(link: string) {
    const shortLinkGenerated = this.generateShortCode(link, 8);
    const shortUrl = `fit.ly/${shortLinkGenerated}`;
    console.log('-A-2-----------------------------------');

    try {
    //   const createdUser = await this.prisma.user.create({
        // data: { shortUrl: shortUrl },
    //   });
      // Действия при успешном создании пользователя
    } catch (error) {
      console.error('Ошибка при создании пользователя:', error);
      // Действия при ошибке
    }

    console.log(shortUrl);
    return shortUrl;
  }

  // Функция для генерации короткого URL-кода
  private generateShortCode(longUrl: string, length: number = 8): string {
    const hash = crypto.createHash('sha256').update(longUrl).digest('hex');
    const shortCode = hash.substring(0, length);

    return shortCode;
  }
}
