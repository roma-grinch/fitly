import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from '../services';
import { CreateUserBodyDTO } from '../dto/create-user-body.dto';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async createUser(@Body() body: CreateUserBodyDTO): Promise<boolean> {
    const result = await this.userService.createUser(body);

    return result;
  }
}
