import { IsString } from 'class-validator';

export class CreateUserBodyDTO {
  @IsString()
  readonly name: string;
}
