import { Injectable } from '@nestjs/common';
import { PrismaService } from '@common/prisma';
import { User } from '@prisma/client';
import { OptionalId } from '@common/types';

@Injectable()
export class UserRepository {
  constructor(private prisma: PrismaService) {}

  async createUser(data: OptionalId<User>): Promise<boolean> {
    const user = await this.prisma.user.create({ data });
    return !user ? false : true;
  }
}
