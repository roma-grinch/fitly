import { Injectable } from '@nestjs/common';
import { UserRepository } from '../repositories/user.repository';
import { CreateUserBodyDTO } from '../dto/create-user-body.dto';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async createUser(input: CreateUserBodyDTO): Promise<boolean> {
    const result = await this.userRepository.createUser(input);

    return result;
  }
}
