import EnvConfig from '@common/config/environment.config';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from '@common/prisma';
import { ConfigurationModule, ConfigurationService } from './configurations';
import { LinkModule } from './links';
import { UserModule } from './users';

const modules = [ConfigurationModule, PrismaModule, LinkModule, UserModule];

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [EnvConfig],
    }),
    ...modules,
  ],
  providers: [
    { provide: 'ConfigurationService', useClass: ConfigurationService },
  ],
})
export class AppModule {}
