import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule, ConfigurationService } from './modules';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const config: ConfigurationService = app.get('ConfigurationService');

  await app.listen(Number(config.env.APP_PORT), String(config.env.APP_HOST));
  Logger.log(`Application is running on: ${await app.getUrl()}`);
}

bootstrap();
