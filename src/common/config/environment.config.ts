import { IsNumber, IsOptional, IsString, validateSync } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { registerAs } from '@nestjs/config';

export class EnvironmentConfig {
  @IsString()
  NODE_ENV!: string;

  @IsNumber()
  @IsOptional()
  APP_PORT: number | null;

  @IsString()
  @IsOptional()
  APP_HOST: string | null = '0.0.0.0';

  @IsString()
  DATABASE_URL!: string;
}

export default registerAs('env', function (): EnvironmentConfig {
  const validatedConfig = plainToClass(EnvironmentConfig, process.env, {
    enableImplicitConversion: true,
  });

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return validatedConfig;
});
