import {
  Injectable,
  Logger,
  OnModuleDestroy,
  OnModuleInit,
} from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService
  extends PrismaClient
  implements OnModuleInit, OnModuleDestroy
{
  constructor() {
    super();
  }

  public async onModuleInit() {
    try {
      await this.$connect();
      Logger.log('Prisma is connected');
    } catch (err) {
      Logger.error(`Prisma connection error: ${err}`);
      throw new Error('Unable to connect to the database');
    }
  }

  public async onModuleDestroy() {
    try {
      if (this.$disconnect) {
        await this.$disconnect();
        Logger.log('Prisma is disconnected!');
      }
    } catch (err) {
      Logger.error(`Prisma disconnection error: ${err}`);
      throw new Error('Failed to disconnect from the database');
    }
  }
}
